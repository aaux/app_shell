 /* ===================================================
  * main.js - Main Behaviors
  * =================================================== */

 // GLOBAL VARIABLES
 var global = {};

 global = {
      init: function () {
           global.resizeShellBehaviors();
           global.toggleDetailsPanel();
           global.toggleSidenav();
           global.sidenavPages();
      },
      // RESIZE
      ///////////////
      resizeShellBehaviors: function () {

          if ( $(window).width() < 1280 ) {
               // $(".panel__details").toggleClass("open");
          }

          $(window).resize(function() {
               if ( $(window).width() > 1280 ) {
                    $('.panel__details').removeClass('open');
               }
          });
      },
      toggleDetailsPanel: function () {
          detailsPanel = $('.panel__details');
          $('.toggle-panel').on('click', function() {
               if( $(window).width() > 1280 ) {
                    detailsPanel.toggleClass('closed');
               } else {
                    detailsPanel.toggleClass('open');
               }
          });
      },
      toggleSidenav: function () {
          sideNav = $('.app-shell__sidenav');
          $('.app-shell_sidenav-toggle').on('click', function() {
               // Change icon 
               $('.app-shell_sidenav-toggle .icon').toggleClass('fa-caret-left fa-caret-right');

               // Expanded or Collapsed view
               if (sideNav.hasClass('collapsed-hover')) {
                    // Expand View
                    sideNav.removeClass('collapsed-hover');
                    // put back classes removed from mouseover/leave
                    $('.logo-1').addClass('fadeIn');
                    $('.logo-2').addClass('fadeOut');
               } else {
                    // Collapsed View
                    // fade logo
                    $('.logo-1, .logo-2 ').toggleClass('fadeIn fadeOut');

                    // allow collapse, time logo-2 to delay a bit more until logo-1 gone
                    setTimeout(
                         function() {
                              $('.logo-1, .logo-2').toggle();
                         }, 250
                    );
               }
               // Collapse sidenav
               sideNav.toggleClass('collapsed');
          });
      },
      sidenavPages: function () {
          navItem = $('.nav-workspaces li > a');
          navItem.on('click', function() {
               navItem.removeClass('active');
               $(this).toggleClass('active');
               
               pageID = $(this).data('page');
               $('.workspace-page').removeClass('show-page');
               $('#' + pageID).toggleClass('show-page');
          });
      }
 };

 $(global.init);