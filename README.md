# app_shell
Automation Anywhere App Shell Layout


- download at git clone git@bitbucket.org:aaux/app_shell.git
- npm install
- npm start or npm run serve:dist (for production)

## npm run serve:dist ERROR
- if you get a message in the terminal stating:
gulp did you forget to signal async completion

Check in the javascript/jquery that variables are not starting with "let" or "const". Declaring these seems to cause the error.